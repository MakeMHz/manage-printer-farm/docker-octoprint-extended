FROM nunofgs/octoprint

# Socat config
ENV SOCAT_EXEC "sshpass -p \"raspberry\" ssh -oStrictHostKeyChecking=no pi@10.0.0.78 socat - /dev/ttyUSB0"

# Update
RUN apt-get update

# Install socat
RUN apt-get install -y socat sshpass

# Create socat service for our remote serial port
COPY supervisord.conf /etc/supervisor/supervisord.conf
